/*
 * Aurora Droid
 * Copyright (C) 2019-20, Rahul Kumar Patel <whyorean@gmail.com>
 *
 * Aurora Droid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Aurora Droid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Aurora Droid.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

apply plugin: 'com.android.application'
apply plugin: "androidx.navigation.safeargs"

android {
    compileSdkVersion 30

    defaultConfig {
        multiDexEnabled true

        applicationId "com.aurora.adroid"
        minSdkVersion 21
        targetSdkVersion 30
        versionCode 8
        versionName "1.0.8"

        resConfigs "ar-rSA", "de-rDE", "et-rEE", "fr-rFR", "hi-rIN", "kn-rIN", "ml-rIN", "pa-rIN", "pl-rPL", "pt-rBR","pt-rPT", "ro-rRO", "ru-rRU", "sc-rIT", "tr-rTR", "es-rES", "in-rID", "it-rIT", "zh-rTW"

        vectorDrawables.useSupportLibrary = true

        javaCompileOptions {
            annotationProcessorOptions {
                includeCompileClasspath false
            }
        }
    }

    compileOptions {
        targetCompatibility 1.8
        sourceCompatibility 1.8
    }

    lintOptions {
        warning "MissingTranslation", "GetLocales", "VectorDrawableCompat"
        checkReleaseBuilds false
        abortOnError false
    }

    signingConfigs {
        release
    }

    buildTypes {
        release {
            minifyEnabled true
            zipAlignEnabled true
            shrinkResources true
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            signingConfig signingConfigs.release
            resValue "string", "app_name", "Aurora Droid"
        }
        beta {
            initWith release
            applicationIdSuffix = ".beta"
            resValue "string", "app_name", "Aurora Droid - Beta"
        }
        debug {
            resValue "string", "app_name", "Aurora Droid - Debug"
        }
    }
}

buildscript {
    ext {
        versions = [
                fastAdapter: "5.3.2",
                okhttp3    : "4.3.1",
                glide      : "4.11.0",
                room       : "2.2.6",
                lambok     : "1.18.10",
                butterknife: "10.2.1",
                libsu      : "3.0.2",
                fetch2     : "3.1.4"
        ]
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation group: 'commons-io', name: 'commons-io', version: '2.8.0'
    implementation group: 'org.apache.maven', name: 'maven-artifact', version: '3.6.2'

    //Jetpack-Components
    implementation 'androidx.navigation:navigation-fragment:2.3.3'
    implementation 'androidx.navigation:navigation-ui:2.3.3'

    implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'
    implementation 'androidx.lifecycle:lifecycle-common-java8:2.2.0'

    //AndroidX
    implementation 'androidx.annotation:annotation:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation 'androidx.preference:preference:1.1.1'
    implementation "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"

    //Google's shit
    implementation 'com.google.android.material:material:1.2.1'
    implementation 'com.google.code.gson:gson:2.8.6'

    //Extensions
    implementation "com.mikepenz:fastadapter:${versions.fastAdapter}"
    implementation "com.mikepenz:fastadapter-extensions-diff:${versions.fastAdapter}"
    implementation "com.mikepenz:fastadapter-extensions-scroll:${versions.fastAdapter}"
    implementation "com.mikepenz:fastadapter-extensions-ui:${versions.fastAdapter}"
    implementation "com.mikepenz:fastadapter-extensions-utils:${versions.fastAdapter}"

    implementation 'me.zhanghai.android.fastscroll:library:1.1.4'

    implementation 'io.github.luizgrp.sectionedrecyclerviewadapter:sectionedrecyclerviewadapter:3.1.0'

    //Utils
    implementation 'org.apache.commons:commons-text:1.8'
    implementation 'commons-validator:commons-validator:1.6'
    implementation 'me.dm7.barcodescanner:zxing:1.9.13'

    //Lombok
    compileOnly "org.projectlombok:lombok:1.18.12"
    annotationProcessor "org.projectlombok:lombok:1.18.12"

    //Android-Room
    implementation "androidx.room:room-runtime:${versions.room}"
    annotationProcessor "androidx.room:room-compiler:${versions.room}"

    //OkHTTP3
    implementation "com.squareup.okhttp3:okhttp:${versions.okhttp3}"
    implementation "com.squareup.okhttp3:okhttp-urlconnection:${versions.okhttp3}"

    //RX-Java2
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'
    implementation 'io.reactivex.rxjava2:rxjava:2.2.16'
    implementation 'com.jakewharton.rxrelay2:rxrelay:2.1.1'

    //ButterKnife
    implementation "com.jakewharton:butterknife:${versions.butterknife}"
    annotationProcessor "com.jakewharton:butterknife-compiler:${versions.butterknife}"

    //Glide
    implementation "com.github.bumptech.glide:glide:${versions.glide}"
    implementation "com.github.bumptech.glide:okhttp3-integration:${versions.glide}"
    annotationProcessor "com.github.bumptech.glide:compiler:${versions.glide}"

    //Fetch - Downloader
    implementation "androidx.tonyodev.fetch2:xfetch2:${versions.fetch2}"
    implementation "androidx.tonyodev.fetch2okhttp:xfetch2okhttp:${versions.fetch2}"

    //Lib-SU
    implementation "com.github.topjohnwu.libsu:core:${versions.libsu}"

    //Debug Utils
    debugImplementation 'com.amitshekhar.android:debug-db:1.0.6'
    //debugImplementation 'com.squareup.leakcanary:leakcanary-android:2.1'
}

Properties props = new Properties()
def propFile = new File('signing.properties')
if (propFile.canRead()) {
    props.load(new FileInputStream(propFile))

    if (props != null && props.containsKey('STORE_FILE') && props.containsKey('STORE_PASSWORD') &&
            props.containsKey('KEY_ALIAS') && props.containsKey('KEY_PASSWORD')) {
        android.signingConfigs.release.storeFile = file(props['STORE_FILE'])
        android.signingConfigs.release.storePassword = props['STORE_PASSWORD']
        android.signingConfigs.release.keyAlias = props['KEY_ALIAS']
        android.signingConfigs.release.keyPassword = props['KEY_PASSWORD']
    } else {
        println 'signing.properties found but some entries are missing'
        android.buildTypes.release.signingConfig = null
    }
} else {
    println 'signing.properties not found'
    android.buildTypes.release.signingConfig = null
}

